import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CrearTerapeutaComponent} from "./modulos/terapeuta-module/componentes/crear-terapeuta/crear-terapeuta.component";
import {LoginComponent} from "./modulos/login-module/componentes/login/login.component";
import {ListarTerapeutaComponent} from "./modulos/terapeuta-module/componentes/listar-terapeuta/listar-terapeuta.component";
import {EditarTerapeutaComponent} from "./modulos/terapeuta-module/componentes/editar-terapeuta/editar-terapeuta.component";
import {VerTerapeutaComponent} from "./modulos/terapeuta-module/componentes/ver-terapeuta/ver-terapeuta.component";
import {HomeComponent} from "./modulos/home-module/componentes/home-admin";
import {CrearSonidoComponent} from "./modulos/sonidos-module/componentes/crear-sonido/crear-sonido.component";
import {ListarSecuenciaComponent} from "./modulos/secuencia-module/componentes/listar-secuencia/listar-secuencia.component";
import {CrearSecuenciaComponent} from "./modulos/secuencia-module/componentes/crear-secuencia/crear-secuencia.component";
import {VerSecuenciaComponent} from "./modulos/secuencia-module/componentes/ver-secuencia/ver-secuencia.component";
import {AuthGuard} from "./modulos/login-module/guardias/auth.guard";
import {InicioComponent} from "./modulos/inicio-module/componentes/inicio/inicio.component";
import {HomeEstudianteComponent} from "./modulos/home-module/componentes/home-estudiante/home-estudiante.component";
import {CombinacionesComponent} from "./modulos/estudiante-module/componentes/combinaciones/combinaciones.component";
import {SecuenciasComponent} from "./modulos/estudiante-module/componentes/secuencias/secuencias.component";

const routes: Routes = [

  {
    path: '', component: InicioComponent, pathMatch: 'full'

  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'crear-terapeuta', component: CrearTerapeutaComponent
  },

  {
    path: 'home-admin', component: HomeComponent, canActivate: [AuthGuard]
  },
  {
    path: 'listar-terapeuta', component: ListarTerapeutaComponent, canActivate: [AuthGuard]
  },
  {
    path: 'editar/:id',component: EditarTerapeutaComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ver-terapeuta/:id', component: VerTerapeutaComponent, canActivate: [AuthGuard]
  },
  {
    path:'crear-sonido', component: CrearSonidoComponent, canActivate: [AuthGuard]
  },
  {
    path:'listar-secuencia',component:ListarSecuenciaComponent, canActivate: [AuthGuard]
  },
  {
    path:'crear-secuencia',component:CrearSecuenciaComponent, canActivate: [AuthGuard]
  },
  {
    path: 'ver-secuencia/:id', component: VerSecuenciaComponent, canActivate: [AuthGuard]
  },
  {
    path: 'estudiante', component: HomeEstudianteComponent,
  },
  {
    path:'secuencias', component: SecuenciasComponent
  },
  {
    path: 'combinaciones', component: CombinacionesComponent
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
