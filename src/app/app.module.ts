import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {ToasterModule} from 'angular2-toaster';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {DataTableModule, InputTextModule, SharedModule} from "primeng/primeng";
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';
import {DataViewModule} from 'primeng/dataview';
import {DropdownModule} from 'primeng/dropdown';
import {CalendarModule} from 'primeng/calendar';
import { ngfModule, ngf } from "angular-file"
import {FileUploadModule} from "ng2-file-upload";
import {ToggleButtonModule} from 'primeng/togglebutton';
import {InputSwitchModule} from 'primeng/inputswitch';
import {LightboxModule} from 'primeng/lightbox';
import {MenubarModule} from 'primeng/menubar';
import {PanelModule} from 'primeng/panel';
import {OverlayPanelModule} from 'primeng/overlaypanel';
import {RadioButtonModule} from 'primeng/radiobutton';
//componentes
import { CrearTerapeutaComponent } from './modulos/terapeuta-module/componentes/crear-terapeuta/crear-terapeuta.component';
import { LoginComponent } from './modulos/login-module/componentes/login/login.component';
import {TerapeutaService} from "./modulos/terapeuta-module/terapeuta.service";
import { FormularioTerapeutaComponent } from './modulos/terapeuta-module/componentes/formulario-terapeuta/formulario-terapeuta.component';
import { EditarTerapeutaComponent } from './modulos/terapeuta-module/componentes/editar-terapeuta/editar-terapeuta.component';
import { VerTerapeutaComponent } from './modulos/terapeuta-module/componentes/ver-terapeuta/ver-terapeuta.component';
import { ListarTerapeutaComponent } from './modulos/terapeuta-module/componentes/listar-terapeuta/listar-terapeuta.component';
import {HomeComponent} from "./modulos/home-module/componentes/home-admin";
import { CrearSonidoComponent } from './modulos/sonidos-module/componentes/crear-sonido/crear-sonido.component';
import { ListarSecuenciaComponent } from './modulos/secuencia-module/componentes/listar-secuencia/listar-secuencia.component';
import { CrearSecuenciaComponent } from './modulos/secuencia-module/componentes/crear-secuencia/crear-secuencia.component';
import { FormularioSecuenciaComponent } from './modulos/secuencia-module/componentes/formulario-secuencia/formulario-secuencia.component';
import { VerSecuenciaComponent } from './modulos/secuencia-module/componentes/ver-secuencia/ver-secuencia.component';
import { BarraAdminComponent } from './modulos/barra-module/componentes/barra-admin/barra-admin.component';
import { InicioComponent } from './modulos/inicio-module/componentes/inicio/inicio.component';
import { HomeEstudianteComponent } from './modulos/home-module/componentes/home-estudiante/home-estudiante.component';
import { CombinacionesComponent } from './modulos/estudiante-module/componentes/combinaciones/combinaciones.component';
import { SecuenciasComponent } from './modulos/estudiante-module/componentes/secuencias/secuencias.component';
import {UnionComponent} from "./modulos/estudiante-module/componentes/union/union.component";
import { BarraEstudianteComponent } from './modulos/barra-module/componentes/barra-estudiante/barra-estudiante.component';
import {CardModule} from 'primeng/card';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import { BreadcrumbComponent } from './modulos/barra-module/componentes/breadcrumb/breadcrumb.component';
import { InstruccionesComponent } from './modulos/barra-module/componentes/instrucciones/instrucciones.component';


@NgModule({
  declarations: [
    AppComponent,
    CrearTerapeutaComponent,
    LoginComponent,
    FormularioTerapeutaComponent,
    EditarTerapeutaComponent,
    VerTerapeutaComponent,
    ListarTerapeutaComponent,
    HomeComponent,
    CrearSonidoComponent,
    ListarSecuenciaComponent,
    CrearSecuenciaComponent,
    FormularioSecuenciaComponent,
    VerSecuenciaComponent,
    BarraAdminComponent,
    InicioComponent,
    HomeEstudianteComponent,
    CombinacionesComponent,
    SecuenciasComponent,
    UnionComponent,
    BarraEstudianteComponent,
    BreadcrumbComponent,
    InstruccionesComponent
  ],
  imports: [
    FileUploadModule ,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToasterModule.forRoot(),
    BrowserAnimationsModule,
    InputTextModule,
    TableModule,
    ButtonModule,
    BreadcrumbModule,
    ToastModule,
    DropdownModule,
    CalendarModule,
    ngfModule,
    DataTableModule,
    SharedModule,
    ToggleButtonModule,
    InputSwitchModule,
    LightboxModule,
    MenubarModule,
    DataViewModule,
    PanelModule,
    CardModule,
    OverlayPanelModule,
    RadioButtonModule
  ],
  providers: [
    TerapeutaService,
    ToastModule  
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
