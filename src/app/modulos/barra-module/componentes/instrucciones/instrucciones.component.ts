import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-instrucciones',
  templateUrl: './instrucciones.component.html',
  styleUrls: ['./instrucciones.component.css']
})
export class InstruccionesComponent implements OnInit {

  tipo;

  constructor() { }

  ngOnInit() {
    let test=localStorage.getItem('rutas')

    //console.log(test)

    if(test === '[{"key":"Inicio","value":"/estudiante"},{"key":"Secuencias Visual-Auditivas","value":"/secuencias"}]'){
      this.tipo='secuencia'
    }else if(test=== '[{"key":"Inicio","value":"/estudiante"},{"key":"Asociaciones Imagen-Sonido","value":"/combinaciones"}]'){
      this.tipo ='asociacion'
    }

    //console.log(this.tipo)
  }




}
