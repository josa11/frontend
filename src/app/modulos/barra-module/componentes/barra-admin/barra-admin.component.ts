import { Component, OnInit } from '@angular/core';
import {MenuItem} from "primeng/api";
import {SesionService} from "../../../login-module/sesion.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-barra-admin',
  templateUrl: './barra-admin.component.html',
  styleUrls: ['./barra-admin.component.css']
})
export class BarraAdminComponent implements OnInit {

  currentUser;
  items;
  home

  constructor(private sesion: SesionService, private router: Router) {
    this.currentUser = JSON.parse(sessionStorage.getItem('sessionInfo'));
    //console.log(this.currentUser.account.nombreUsuario)

  }

  ngOnInit() {
    this.items = [
      {
        label: 'Secuencia',
        items: [
          {
          label: 'Listar Secuencia',
          icon: 'pi pi-bars',
          url:'/listar-secuencia'
         },
          {
            label: 'Nueva Secuencia',
            icon: 'pi pi-images',
            url:'/crear-secuencia'
          },

        ]
      },
      //{
        //label: 'Sonidos',
        //items: [
          //{
            //label: 'Generar Sonido',
            //icon: 'pi pi-fw pi-volume-up',
            //url:'/crear-sonido'
          //}
        //]
      //},
      {
        label: 'Terapeuta',
        items: [
          {
            label: 'Listar Terapeuta',
            icon: 'pi pi-users',
            url: '/listar-terapeuta'
          },
          {
            label: 'Nuevo Terapeuta',
            icon: 'pi pi-user-plus',
            url:'/crear-terapeuta'
          },

        ]
      },

    ];
    this.home = {icon: 'pi pi-home-admin'};
  }

  logout(){
    this.sesion.destroySession();
    this.router.navigateByUrl('/');
  }

  iniciar(){
    this.router.navigateByUrl('/home-admin');
  }



}
