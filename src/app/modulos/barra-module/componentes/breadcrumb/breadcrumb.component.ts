import { Component, OnInit } from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {

  rutasbc=[]

  constructor(private router: Router) { }

  ngOnInit() {
    this.rutasbc=JSON.parse(localStorage.getItem('rutas'))

  }

  ir(ruta){
    //console.log(ruta)
    this.router.navigateByUrl('/'+ruta);
  }
}
