import { Component, OnInit } from '@angular/core';
import {SesionService} from "../../../login-module/sesion.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-barra-estudiante',
  templateUrl: './barra-estudiante.component.html',
  styleUrls: ['./barra-estudiante.component.css']
})
export class BarraEstudianteComponent implements OnInit {

  items;
  bc=[];
  key=[]
  tipo
  mostrar

  private sesion: any;
  constructor(private router: Router) {  }

  ngOnInit() {
    this.sesion=  JSON.parse(sessionStorage.getItem( 'sessionInfo' ));
    let test=localStorage.getItem('rutas')
    //console.log(test)

    if(this.sesion===null){
      this.items = [
        {
          label: 'Asociaciones Imagen-Sonido',
          icon: 'pi pi-images',
          url: '/combinaciones'
        },
        {
          label: 'Secuencias Visual-Auditiva',
          icon: 'pi pi-images',
          url: '/secuencias'
        },
      ];
    }else{
      this.items = [
        {
          label: 'Asociaciones Imagen-Sonido',
          icon: 'pi pi-images',
          url: '/combinaciones'
        },
        {
          label: 'Secuencias Visual-Auditiva',
          icon: 'pi pi-images',
          url: '/secuencias'
        },
        {
          label: 'Administrar',
          icon: 'pi pi-images',
          url: '/home-admin'
        },

      ];
    }

    if(test == '[{"key":"Inicio","value":"/estudiante"}]' ){
      this.mostrar=false
    }else {
      this.mostrar=true
    }
  }

  logout(){
    this.router.navigateByUrl('/');
  }

  iniciar(){
    this.router.navigateByUrl('/estudiante');
  }

  limpiar(item, ruta){
    this.bc = [{key:'Incio', value:'Inicio'},{key:item, value: ruta}]
    localStorage.setItem('rutas', JSON.stringify(this.bc));
  }



}
