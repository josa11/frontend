import { Injectable } from '@angular/core';
import {CanActivate,   Router} from '@angular/router';
import {SesionService} from "../sesion.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private session: SesionService) { }

  canActivate(){
    if(this.session.getSessionInfo().loggedIn) return true;

    this.router.navigate(['/login'])
    return false;
  }

}
