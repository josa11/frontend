import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {Terapeuta} from "../terapeuta-module/interfaces/terapeuta.model";

@Injectable({
  providedIn: 'root'
})
export class SesionService {

  constructor() { }

  setUserInfo( account, loggedIn ) {

    const sessionInfo = {
      account: account,
      loggedIn: loggedIn
    };
    sessionStorage.setItem( 'sessionInfo', JSON.stringify( sessionInfo ) );
  }




  getSessionInfo() {
    const obj = sessionStorage.getItem( 'sessionInfo' );
    if ( obj != null ) {
      let sessionInfo = JSON.parse( obj );
      return sessionInfo;
    } else {
      const sessionInfo = {
        terapeuta: new Terapeuta(),
        loggedIn: false
      };

      sessionStorage.setItem( 'sessionInfo', JSON.stringify( sessionInfo ) );

      return sessionInfo;
    }
  }

  destroySession() {
    sessionStorage.clear();
  }



}
