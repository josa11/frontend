import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {Terapeuta} from "../terapeuta-module/interfaces/terapeuta.model";
import {environment} from "../../../environments/environment";
import {HttpClient} from "../../../../node_modules/@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor( private http: HttpClient) { }

  //private baseUrl = 'http://52.167.3.23/';


  

  login(datos) {
    return this.http.put(environment.apiBaseUrl + '/login/', datos );
  }

  reset(datos):Observable<Terapeuta>{
    return this.http.put( environment.apiBaseUrl+`/reset/`, datos );
  }


}
