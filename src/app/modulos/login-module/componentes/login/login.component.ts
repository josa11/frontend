import { Component, OnInit } from '@angular/core';
import {Terapeuta} from "../../../terapeuta-module/interfaces/terapeuta.model";
import {MessageService} from "primeng/api";
import {LoginService} from "../../login.service";
import {SesionService} from "../../sesion.service";
import {Router} from "@angular/router";
import {TerapeutaService} from "../../../terapeuta-module/terapeuta.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {

  reset:boolean = false;
  deshabilitar:boolean = false;
  _terapeuta: Terapeuta;

  constructor(private loginService: LoginService, private messageService: MessageService,
              private sesionService: SesionService, private router: Router,
              private terapeutaService: TerapeutaService) { }
  private terapeuta: Terapeuta = {
    nombreUsuario: '',
    contrasena: ''
  };

  public isError = false;

  ngOnInit() {
    sessionStorage.clear();
  }

  loguearse(){
    this.loginService.login(this.terapeuta).subscribe(r=>{
      //console.log(r);
      this.sesionService.setUserInfo(r, true);
      this.router.navigateByUrl('/home-admin')

    },error => {
      this.messageService.add({key: 'myKey1', severity:'error', summary: 'Error', detail: error.error.err });
      //console.log(error.error)
    })
  }

  buscarUsuario(){

    //console.log(this.terapeuta);
    this.loginService.reset({"nombreUsuario":this.terapeuta.nombreUsuario}).subscribe(r=>{
      this._terapeuta= r;
      this.deshabilitar=true;
    },error => {
      this.messageService.add({key: 'myKey1', severity:'error', summary: 'Error', detail: error.error.err });
      //console.log(error.error)
    })
  }


  actualizar(){
    let terapeuta ={
      "nombreUsuario":this._terapeuta.nombreUsuario,
      "contrasena": this.terapeuta.contrasena,
      "nombre":this._terapeuta.nombre,
      "apellido":this._terapeuta.apellido,
      "id":this._terapeuta.id
    }

    this.terapeutaService.editar(terapeuta).subscribe(r=>{
      this.messageService.add({key: 'myKey1', severity:'success', summary: 'Mensaje', detail: "Contraseña Actualizada" });

      this.reset=false;
    },error => {
      this.messageService.add({key: 'myKey1', severity:'error', summary: 'Error', detail: error.error.err });
      console.log(error.error)
    })

  }

}
