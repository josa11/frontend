import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {Terapeuta} from "../../interfaces/terapeuta.model";
import {ActivatedRoute} from "@angular/router";
import {TerapeutaService} from "../../terapeuta.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-formulario-terapeuta',
  templateUrl: './formulario-terapeuta.component.html',
  styleUrls: ['./formulario-terapeuta.component.css']
})
export class FormularioTerapeutaComponent{


  now=new Date();

  @Output() esValidoFormularioTerapeuta : EventEmitter<FormGroup> = new EventEmitter()
  @Input() esFormularioCrear = false
  @Input() esFormularioEditar = false
  @Input() esFormularioVer = false

  @Output() terapeutaSeleccionado: EventEmitter<Terapeuta> = new EventEmitter()

  terapeuta: Terapeuta
  formularioTerapeuta : FormGroup

  mensajesErroresNombreTerapeuta=[];
  mensajesErroresApellidoTerapeuta=[];
  mensajesErroresNombreUsuario=[];
  mensajesErroresContrasena=[];


  private mensajesValidacionNombre={
    required: "El nombre es obligatorio",
    validacionMinLength: "El nombre debe tener al menos 4 caracteres"
  }

  private mensajesValidacionApellido={
    required: "El apellido es obligatorio",
    validacionMinLength: "El apellido debe tener al menos 4 caracteres"
  }
  private mensajesValidacionesNombreUsuario={
    required: "El nombre de usuario es obligatorio",
    validacionMinLength: "El nombre de usuario debe tener al menos 4 caracteres",
    pattern: 'El nombre de usuario es incorrecto'
  }
  private mensajesValidacionesContrasena={
    required: "La conraseña es obligatoria",
    validacionMinLength: "La contraseña debe tener al menos 6 caracteres"
  }

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly activateRoute: ActivatedRoute,
    private readonly terapeutaService: TerapeutaService
  ){ }

  ngOnInit(){
    this.crearFormulario()
    this.escucharFormulario()
    this.verificarTipoFormulario()

  }


  crearFormulario(){
    this.formularioTerapeuta = this.formBuilder.group({
      nombre: ['',[Validators.required, this.validacionMinLength(3)]],
      apellido: ['',[Validators.required, this.validacionMinLength(3)]],
      nombreUsuario: ['',[Validators.required, this.validacionMinLength(3)]],
      contrasena:['',[Validators.required, this.validacionMinLength(5)]]

    })
  }

  escucharFormulario(){
    this.formularioTerapeuta.valueChanges.subscribe(()=>{
      this.esValidoFormularioTerapeuta.emit(this.formularioTerapeuta)
      this.escucharCambiosNombre()
      this.escucharCambiosApellido()
      this.escucharCambiosNombreUsuario()
      this.escucharCambiosContrasena()
      })
  }

  verificarTipoFormulario(){
    if(this.esFormularioCrear){}
    if(this.esFormularioEditar){
      this.buscarTerapeuta();
      this.formularioTerapeuta.get('nombreUsuario').disable()
    }
    if(this.esFormularioVer){
      this.buscarTerapeuta()
      this.formularioTerapeuta.disable()
    }
  }

  buscarTerapeuta(){
    this.obtenerIdTerapeuta().subscribe(respuesta=>{
      this.terapeutaService.obtenerUno(respuesta.get('id')).subscribe(terapeuta=>{
        this.terapeuta = terapeuta;
        this.terapeutaSeleccionado.emit(this.terapeuta);
        this.llenarFormulario(this.terapeuta);
      },
        error =>{
        //console.log('Error al obtener el id del Terapeuta', error);
        })
    })
  }

  llenarFormulario(terapeuta:Terapeuta){
    this.formularioTerapeuta.patchValue({
      nombre: terapeuta.nombre,
      apellido: terapeuta.apellido,
      nombreUsuario: terapeuta.nombreUsuario,
      contrasena: terapeuta.contrasena
    })
    this.terapeuta = terapeuta
  }

  obtenerIdTerapeuta():Observable<any>{
    return this.activateRoute.paramMap
  }

  escucharCambiosNombre(){
    const nombreControl = this.formularioTerapeuta.get('nombre')
    nombreControl.valueChanges.subscribe(valor=>{
      this.setearMensajesErroresNombre(nombreControl)
    })
  }

  escucharCambiosApellido(){
    const apellidoControl = this.formularioTerapeuta.get('apellido')
    apellidoControl.valueChanges.subscribe(valor=>{
      this.setearMensajesErroresApellido(apellidoControl)
    })
  }

  escucharCambiosNombreUsuario(){
    const nombreUsuarioControl = this.formularioTerapeuta.get('nombreUsuario')
    nombreUsuarioControl.valueChanges.subscribe(valor=>{
      this.setearMensajesErroresNombreUsuario(nombreUsuarioControl)
    })
  }

  escucharCambiosContrasena(){
    const contrasenaControl = this.formularioTerapeuta.get('contrasena')
    contrasenaControl.valueChanges.subscribe(valor=>{
      this.setearMensajesErroresContrasena(contrasenaControl)
    })
  }

  setearMensajesErroresNombre(valor: AbstractControl) {
    this.mensajesErroresNombreTerapeuta = [];
    const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
    if (esValidoCampo) {
      this.mensajesErroresNombreTerapeuta = Object.keys(valor.errors).map(atributo => {
        return this.mensajesValidacionNombre[atributo]
      })
    }
  }

  setearMensajesErroresApellido(valor: AbstractControl) {
    this.mensajesErroresApellidoTerapeuta = [];
    const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
    if (esValidoCampo) {
      this.mensajesErroresApellidoTerapeuta = Object.keys(valor.errors).map(atributo => {
        return this.mensajesValidacionApellido[atributo]
      })
    }
  }

  setearMensajesErroresNombreUsuario(valor: AbstractControl) {
    this.mensajesErroresNombreUsuario = [];
    const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
    if (esValidoCampo) {
      this.mensajesErroresNombreUsuario = Object.keys(valor.errors).map(atributo => {
        return this.mensajesValidacionesNombreUsuario[atributo]
      })
    }
  }

  setearMensajesErroresContrasena(valor: AbstractControl){
    this.mensajesErroresContrasena =[];
    const  esValidoCampo = (valor.dirty || valor.touched) && valor.errors
    if(esValidoCampo){
      this.mensajesErroresContrasena = Object.keys(valor.errors).map(atributo =>{
        return this.mensajesValidacionesContrasena[atributo]
      })
    }
  }

  validacionMinLength(longitud: number): ValidatorFn {
    return (valorInput: AbstractControl): { [atributo: string]: boolean } | null => {
      if (valorInput.value.length > longitud) {
        return null
      }
      return { 'validacionMinLength': true }
    }
  }

}
