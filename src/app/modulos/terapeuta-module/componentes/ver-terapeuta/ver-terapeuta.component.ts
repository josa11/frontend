import { Component, OnInit } from '@angular/core';
import {Terapeuta} from "../../interfaces/terapeuta.model";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-ver-terapeuta',
  templateUrl: './ver-terapeuta.component.html',
  styleUrls: ['./ver-terapeuta.component.css']
})
export class VerTerapeutaComponent implements OnInit {

  terapeuta: Terapeuta={};
  formularioVerDetalle: FormGroup
  bc=[]

  constructor(
    private readonly formbuilder: FormBuilder,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.bc.push({key:'Inicio', value:'/home-admin'}, {key:'Listar Terapeuta', value:'/listar-terapeuta'},{key:'Ver Terapeuta', value:'/ver'})
    localStorage.setItem('rutas',JSON.stringify(this.bc))
    this.crearFormulario();
  }

  private crearFormulario() {
    this.formularioVerDetalle = this.formbuilder.group({})
  }

  escucharFormulario(eventoFormulario: FormGroup){
    this.formularioVerDetalle = eventoFormulario
  }

  escucharTerapeuta(terapeuta: Terapeuta){
    this.terapeuta = terapeuta
  }

  irListar(){
    this.router.navigate(['/listar-terapeuta'])
  }
}
