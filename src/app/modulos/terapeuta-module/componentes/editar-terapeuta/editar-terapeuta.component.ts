import { Component, OnInit } from '@angular/core';
import {Terapeuta} from "../../interfaces/terapeuta.model";
import {FormBuilder, FormGroup} from "@angular/forms";
import {TerapeutaService} from "../../terapeuta.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ToasterService} from "angular2-toaster";
import {TIEMPO_ESPERA} from "../../toaster-time";
@Component({
  selector: 'app-editar-terapeuta',
  templateUrl: './editar-terapeuta.component.html',
  styleUrls: ['./editar-terapeuta.component.css'],
  providers:[ToasterService]
})
export class EditarTerapeutaComponent {

  terapeuta : Terapeuta ={};
  esFormularioValido = false;
  formularioEditarTerapeuta : FormGroup;
  bc=[]


  constructor(
    private readonly terapeutaService:TerapeutaService,
    private readonly formBuilder : FormBuilder,
    private readonly activateRoute : ActivatedRoute,
    private router : Router,
    private readonly toasterService : ToasterService
  ) { }

  ngOnInit(){
    this.bc.push({key:'Inicio', value:'/home-admin'}, {key:'Listar Terapeuta', value:'/listar-terapeuta'},{key:'Editar Terapeuta', value:'/editar'})
    localStorage.setItem('rutas',JSON.stringify(this.bc))
    this.crearFormulario();
  }

  editarUsuario() {
    this.terapeuta.nombre = this.formularioEditarTerapeuta.get('nombre').value;
    this.terapeuta.apellido = this.formularioEditarTerapeuta.get('apellido').value;
    this.terapeuta.nombreUsuario = this.formularioEditarTerapeuta.get('nombreUsuario').value;
    this.terapeuta.contrasena = this.formularioEditarTerapeuta.get('contrasena').value;
    //console.log(this.terapeuta);
    this.terapeutaService.editar(this.terapeuta).subscribe(terapeutaCreado=>{
        this.terapeuta.id = terapeutaCreado.id;
      //  console.log('Entramos');
        //this.toasterService.pop('success','Éxito', 'Usuario Editado Correctamente');
        setTimeout(()=>{
          this.irListar()
        },
          TIEMPO_ESPERA)
            },
          error=>{
          //this.toasterService.pop('warning','Advertencia','Error al editar terapeuta');
    })
  }


  crearFormulario(){
    this.formularioEditarTerapeuta = this.formBuilder.group({})
  }

  escucharTerapeuta(terapeuta: Terapeuta){
    this.terapeuta=terapeuta;
  }

  escucharFormulario(eventoFormulario: FormGroup) {
    this.formularioEditarTerapeuta = eventoFormulario
    this.esFormularioValido=this.formularioEditarTerapeuta.valid
  }



  irListar(){
    this.router.navigate(['/listar-terapeuta'])
  }




}
