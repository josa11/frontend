import { Component, OnInit } from '@angular/core';
import {TerapeutaService} from "../../terapeuta.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Terapeuta} from "../../interfaces/terapeuta.model";
import {Router} from "@angular/router";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-crear-terapeuta',
  templateUrl: './crear-terapeuta.component.html',
  styleUrls: ['./crear-terapeuta.component.css'],
  providers:[TerapeutaService, MessageService]
})
export class CrearTerapeutaComponent implements OnInit {

  terapeuta: Terapeuta = {};
  esFormularioValido = false;
  formularioCrearTerapeuta: FormGroup;
  bc=[]

  constructor(
    private readonly terapeutaService: TerapeutaService,
    private readonly formBuilder : FormBuilder,
    private router: Router,
    private messageService: MessageService){}

    ngOnInit(){
      this.bc.push({key:'Inicio', value:'/home-admin'}, {key:'Listar Terapeuta', value:'/listar-terapeuta'},{key:'Crear Terapeuta', value:'/ver-secuencia'})
      localStorage.setItem('rutas',JSON.stringify(this.bc))
    this.crearFormulario()

    }

    crearTerapeuta(){
      this.terapeuta.nombre = this.formularioCrearTerapeuta.get('nombre').value;
      this.terapeuta.apellido = this.formularioCrearTerapeuta.get('apellido').value;
      this.terapeuta.nombreUsuario = this.formularioCrearTerapeuta.get('nombreUsuario').value;
      this.terapeuta.contrasena = this.formularioCrearTerapeuta.get('contrasena').value;

      //console.log(this.terapeuta)

      this.terapeutaService.crear(this.terapeuta).subscribe( ()=>{
        this.router.navigate(['/listar-terapeuta'])
        this.messageService.add({key: 'myKey1',severity:'success', summary:'Registro Exitoso', detail:'Terapeuta Creado'});

        },
        error =>{
        this.messageService.add({key: 'myKey1',severity:'error', summary:'Registro Fallido', detail:'No se creo terapeuta'});
        //console.log(this.terapeuta.id + 'aaa')
      })

    }

    crearFormulario(){
    this.formularioCrearTerapeuta = this.formBuilder.group({})
    }

    escucharFormulario(eventoFormulario: FormGroup){
      this.formularioCrearTerapeuta = eventoFormulario
      this.esFormularioValido = this.formularioCrearTerapeuta.valid
    }

    escucharTerapeuta(terapeuta:Terapeuta){
      this.terapeuta = terapeuta
     // console.log(this.terapeuta)
    }

    irListar(){
      this.router.navigate(['/listar-terapeuta'])

    }



}
