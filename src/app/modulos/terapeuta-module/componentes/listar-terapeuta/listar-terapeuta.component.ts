import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Terapeuta} from "../../interfaces/terapeuta.model";
import {TerapeutaService} from "../../terapeuta.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-listar-terapeuta',
  templateUrl: './listar-terapeuta.component.html',
  styleUrls: ['./listar-terapeuta.component.css']
})
export class ListarTerapeutaComponent implements OnInit {

  arregloTerapeutas : Terapeuta[]=[];
  parametroBusqueda: string
  mensajeArregloVacio: string
  parametroOpcionalId:number
  bc=[]

  constructor(
    private readonly terapeutaService : TerapeutaService,
    private readonly activateRoute : ActivatedRoute,
    private router :Router
  ) {  }

  ngOnInit() {
    this.bc.push({key:'Inicio', value:'/home-admin'}, {key:'Listar Terapeuta', value:'/listar-terapeuta'})
    localStorage.setItem('rutas',JSON.stringify(this.bc))
    this.listarTerapeutas();
    const parametros$ = this.activateRoute.paramMap
    parametros$.subscribe(
      respuestaParam=>{
        this.parametroOpcionalId=Number(respuestaParam.get('id'))
      }
    )
  }
  listarTerapeutas(){
    this.terapeutaService.obtenerTodos().subscribe(terapeutas=>{
      this.arregloTerapeutas = terapeutas

    },
      error =>{
      //console.log("Error al listar usuarios", error);
      })
  }

  eliminarTerapeuta(id:string){
    this.terapeutaService.delete(id).subscribe(()=>{
      this.listarTerapeutas()});
  }


}
