import { Injectable } from '@angular/core';
import {Terapeuta} from "./interfaces/terapeuta.model";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {observable, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TerapeutaService {

  seleccionarTerapeuta: Terapeuta ={

    nombre: '',
    apellido: '',
    nombreUsuario: '',
    contrasena: ''
  };

  constructor(
    private http: HttpClient
  ) {}

  private getHeaders() {
    const headers = new Headers();
    headers.append( 'Accept', 'application/json' );
    headers.append( 'Content-Type', 'application/json' );
    return headers;
  }

  crear(terapeuta: Terapeuta):Observable<Terapeuta>{
    return this.http.post(environment.apiBaseUrl + '/terapeuta/', terapeuta)
  }
  obtenerUno(id: string): Observable<Terapeuta>{
    return this.http.get(environment.apiBaseUrl + `/terapeuta/${id}`)
  }
  obtenerTodos():Observable<any>{
    return  this.http.get(environment.apiBaseUrl + '/terapeuta');
  }
  editar(terapeutaEditar: Terapeuta):Observable<Terapeuta>{
    const id= terapeutaEditar.id;
    return this.http.put(environment.apiBaseUrl+`/terapeuta/${id}`, terapeutaEditar);
  }
  delete(id: string){
    return this.http.delete(environment.apiBaseUrl+ `/terapeuta/${id}`)
  }




}
