import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';


@Component({
  selector: 'estudiante',
  templateUrl: 'home.component.html',
  styleUrls: ['./home.component.css']})
export class HomeComponent implements OnInit{


  bc=[]

  constructor(
    private router: Router,
    public desinfectante: DomSanitizer
  ){}


  ngOnInit(){
    this.bc.push({key:'Inicio', value:'/home-admin'})
    localStorage.setItem('rutas',JSON.stringify(this.bc))
  }

}
