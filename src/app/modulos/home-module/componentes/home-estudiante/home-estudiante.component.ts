import { Component, OnInit } from '@angular/core';
import {Secuencia} from "../../../secuencia-module/interfaces/secuencia.model";
import {SecuenciaService} from "../../../secuencia-module/secuencia.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-home-estudiante',
  templateUrl: './home-estudiante.component.html',
  styleUrls: ['./home-estudiante.component.css']
})
export class HomeEstudianteComponent implements OnInit {

  arregloSecuencias : Secuencia[]=[];
  combinaciones=[];
  secuencias=[];
  bc=[]

  constructor(
    private readonly secuenciaService: SecuenciaService,
    private activateRoute: ActivatedRoute,
  ) { }


  ngOnInit() {
    this.obtenerSecuencias()
    this.bc.push({key:'Inicio', value:'/estudiante'})
    localStorage.setItem('rutas',JSON.stringify(this.bc))
  }

  obtenerSecuencias(){

    this.secuenciaService.obtenerTodos().subscribe(secuencias => {
        this.arregloSecuencias = secuencias;
        //console.log(this.arregloSecuencias);
        this.arregloSecuencias.map(item=>{
          console.log(item)
          if(item.tipo==" Asociación Imagen-Sonido" && item.estado=='Activo'){
            this.combinaciones.push(item)
          }else if(item.tipo=="Secuencia Visual Auditiva" && item.estado=='Activo'){
            this.secuencias.push(item)
          }
        })

        localStorage.setItem('combinaciones', JSON.stringify(this.combinaciones));
        localStorage.setItem('secuencias', JSON.stringify(this.secuencias));

        //console.log(this.combinaciones);
        //console.log(this.secuencias);

      },
      error => {
        //console.log("Error al listar", error);
      })

  }
}
