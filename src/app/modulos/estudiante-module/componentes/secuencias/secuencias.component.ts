import { Component, OnInit } from '@angular/core';
import {SecuenciaService} from "../../../secuencia-module/secuencia.service";
import {MessageService} from "../../../../../../node_modules/primeng/api";

@Component({
  selector: 'app-secuencias',
  templateUrl: './secuencias.component.html',
  styleUrls: ['./secuencias.component.css'],
  providers: [MessageService]
})
export class SecuenciasComponent implements OnInit {

  arregloCombinaciones=[];
  fecha;
  mes;
  seleccionado: any;
  imagenesForm=[];
  pathImagenes=[];
  sonidosForm=[];
  pathSonidos=[];
  sonidosSort;
  imagenesSort;
  hideme = {};
  contador=[];
  ganaste=false;
  practica=false;
  imagenesOrdenas=[];
  sonidosOrdenados=[];
  count=0;
  bc=[]
  ruta= 'Secuencias Visual-Auditiva'
  key=[]
  error= false
  audio = new Audio();

  constructor(
    private secuenciaService: SecuenciaService,
    private messageService: MessageService,
  ) {
    this.hideme = {};
  }

  ngOnInit() {
    this.obtenerSecuencias();

    this.bc.push({key:'Inicio', value:'/estudiante'}, {key:'Secuencias Visual-Auditivas', value:'/secuencias'})
    localStorage.setItem('rutas',JSON.stringify(this.bc))

  }

  obtenerSecuencias(){
    this.arregloCombinaciones=JSON.parse(localStorage.getItem('secuencias'))
    //console.log(this.arregloCombinaciones)
    this.arregloCombinaciones.map(item=>{
      this.fecha= item.createdAt;
      this.mes=new Date(this.fecha).getMonth()
      //console.log(this.fecha);
    })
  }


  selectCombinacion(event: Event, combinacion) {
    this.imagenesForm=[];
    this.pathImagenes=[];
    this.sonidosForm=[];
    this.pathSonidos=[];
    this.seleccionado = combinacion;
    this.practica = true;
    this.imagenesSort=[];
    this.hideme = {};
    this.ganaste=false;
    this.contador=[];
    this.count=0;

    //console.log(this.seleccionado);

    this.seleccionado.imagenes.map((item, index)=>{
     // console.log(item);
      this.secuenciaService.obtenerImagen(item.id).subscribe(respuesta=>{
        this.imagenesForm = respuesta;
        //let contador = this.imagenesForm.length
        //console.log(this.imagenesForm);
        this.imagenesForm.map(item=>{
          console.log(item)
          let imagenes = '..\\..\\..\\..\\..'+  item.fullPath.substring(55)
          //this.pathImagenes.push({"path":imagenes,"posicion":index});

          //let imagenes = '.'+  item.fullPath.substring(25)
          console.log(imagenes)
          this.pathImagenes.push({"path":imagenes,"posicion":index});

          this.imagenesOrdenas= this.pathImagenes.sort((a,b)=>{
            return a.posicion-b.posicion;
          })
          //console.log(this.imagenesOrdenas)
        })
        this.imagenesSort  = this.pathImagenes.map((a) => ({sort: Math.random(), value: a})).sort((a, b) => a.sort - b.sort).map((a) => a.value);

        //console.log(this.imagenesSort)
      })
    })

    this.seleccionado.sonidos.map((item, index)=>{
      //console.log(item.id)
      this.secuenciaService.obtenerSonido(item.id).subscribe(respuesta=>{
        this.sonidosForm = respuesta;
        //console.log(this.sonidosForm);
        this.sonidosForm.map((item)=>{
          let sonidos = '..\\..\\..\\..\\..'+  item.fullPath.substring(55)
          //let sonidos = '.' + item.fullPath.substring(25)
          this.pathSonidos.push({"path":sonidos,"posicion":index});

          this.sonidosOrdenados= this.pathSonidos.sort((a,b)=>{
            return a.posicion-b.posicion;
          })
          //console.log(this.sonidosOrdenados)
        })
      })
    })

  }


  contadorMouse(i){

    //this.hideme={};
    //console.log(i);

    if(i==this.count){
      //console.log(i, this.count);
      this.hideme[i] = !this.hideme[i];
      this.count++;
      this.contador.push(i);
      if(this.contador.length === this.pathImagenes.length){
        //console.log("ganamos")
        setTimeout(()=>{
          this.ganaste= true;
          //this.playAudio("../../assets/SonidosApp/Ganaste.mp3");
          setTimeout(()=>{
            this.seleccionado=null;
            this.practica=false
            this.ganaste=false
          }, 4500)
        }, 4500)
      }
    }else{
      //console.log("Escoge otra imagen")
      //this.messageService.add({key: 'myKey1', severity:'info', summary: 'Mensaje', detail: "Imagen Incorrecta" });
      this.error=true
      setTimeout(()=>{
        this.error=false
      }, 1500)
      this.playAudio("../../assets/SonidosApp/Error.mp3");
    }


  }

  combinaciones(){
    //console.log('entramos')
    this.seleccionado=null;
    this.ganaste=false
  }

  playAudio(src){
    //console.log(src);
    this.audio.src = src;
    this.audio.load();
    this.audio.play();
  }	

  stopAudio(){
    if(this.audio!=null) {
      this.audio.pause();
    }else {
      //console.log('No hay sonido')
    }

  }



}
