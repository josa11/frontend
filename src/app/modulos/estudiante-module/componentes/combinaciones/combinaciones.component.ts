import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SecuenciaService} from "../../../secuencia-module/secuencia.service";
import {ActivatedRoute} from "@angular/router";

import {DomSanitizer} from "@angular/platform-browser";
import {MessageService} from "../../../../../../node_modules/primeng/api";

@Component({
  selector: 'app-combinaciones',
  templateUrl: './combinaciones.component.html',
  styleUrls: ['./combinaciones.component.css'],
  providers: [MessageService]
})
export class CombinacionesComponent implements OnInit {

  arregloCombinaciones=[];
  seleccionado: any;
  imagenes=[];
  sonidos =[];
  id=[];
  url;
  fecha;
  mes;
  imagenesForm=[];
  pathImagenes=[];
  sonidosForm=[];
  pathSonidos=[];
  sonidosSort;
  imagenesSort;
  posicionImagen;
  posicionSonido;
  isActive = false;
  arrayPosicionImagen=[];
  arrayPosicionSonido=[];
  prueba;
  ganaste:boolean=false;
  audio = new Audio();
  ruta= 'Asociaciones Imagen-Sonido'
  key=[]
  error=false


  x1c;
  y1c;
  x2c;
  y2c;

  coordenadas=[];
  //coordenadas=[];
  juego:boolean=false;
  imageSonido=[];

  imagenesOrdenas=[];
  sonidosOrdenados=[];
  fallaste;
  bc=[]

  constructor(
    private secuenciaService: SecuenciaService,
    public desinfectante: DomSanitizer,
    private activateRoute: ActivatedRoute,
    private messageService: MessageService,
  ) { }


  ngOnInit() {

    this.obtenerSecuencias();

    this.bc.push({key:'Inicio', value:'/estudiante'}, {key:'Asociaciones Imagen-Sonido', value:'/combinaciones'})
    localStorage.setItem('rutas',JSON.stringify(this.bc))

  }

  selectCombinacion(event: Event, combinacion) {

    this.imagenesForm=[];
    this.pathImagenes=[];
    this.sonidosForm=[];
    this.pathSonidos=[];
    this.arrayPosicionImagen=[];
    this.arrayPosicionSonido=[];
    this.imagenes=[];
    this.sonidos =[];
    this.id=[];
    this.coordenadas=[];
    this.imageSonido=[];
    this.imagenesOrdenas=[];
    this.sonidosOrdenados=[];
    this.juego=false;
    this.x1c=null;
    this.y1c=null;
    this.x2c=null;
    this.y2c=null;


    this.seleccionado = combinacion;


    //console.log("seleccionado",this.seleccionado);

    this.seleccionado.imagenes.map((item, index)=>{

      this.secuenciaService.obtenerImagen(item.id).subscribe(respuesta=>{
        this.imagenesForm = respuesta;
        //let contador = this.imagenesForm.length

        this.imagenesForm.map(item=>{
          
          //let imagenes = '..\\..\\..\\..\\..'+  item.fullPath.substring(55)
          let imagenes = '.'+  item.fullPath.substring(25)
          console.log(imagenes)
          this.pathImagenes.push({"path":imagenes,"posicion":index});
          //console.log(this.pathImagenes)
          this.imagenesOrdenas= this.pathImagenes.sort((a,b)=>{
            return a.posicion-b.posicion;
          })
         // console.log(this.imagenesOrdenas)
        })

        this.imagenesSort  = this.pathImagenes
          .map((a) => ({sort: Math.random(), value: a}))
          .sort((a, b) => a.sort - b.sort)
          .map((a) => a.value)


        //buscar
        //
        //console.log(res);
      })
    })

    this.seleccionado.sonidos.map((item, index)=>{
      //console.log(item.id)

      this.secuenciaService.obtenerSonido(item.id).subscribe(respuesta=>{
        this.sonidosForm = respuesta;
        //console.log(this.sonidosForm);
        this.sonidosForm.map((item)=>{
          //let sonidos = '..\\..\\..\\..\\..'+  item.fullPath.substring(55)
          let sonidos = '.'+  item.fullPath.substring(25)
          this.pathSonidos.push({"path":sonidos,"posicion":index});
          //console.log(this.pathSonidos);
          this.sonidosOrdenados= this.pathSonidos.sort((a,b)=>{
            return a.posicion-b.posicion;
          })
          //console.log(this.sonidosOrdenados)
        })
        this.sonidosSort  = this.pathSonidos
          .map((a) => ({sort: Math.random(), value: a}))
          .sort((a, b) => a.sort - b.sort)
          .map((a) => a.value)
      })
    })


  // console.log(this.imageSonido)

  }

  obtenerSecuencias(){
    this.arregloCombinaciones=JSON.parse(localStorage.getItem('combinaciones'))
    //console.log(this.arregloCombinaciones)
    this.arregloCombinaciones.map(item=>{
      this.fecha= item.createdAt;
      this.mes=new Date(this.fecha).getMonth()
      //console.log(this.fecha);
    })
  }

  playAudio(src){
    //console.log(src);
    this.audio.src = src;
    this.audio.load();
    this.audio.play();
  }

  stopAudio(){

    if(this.audio!=null) {
      this.audio.pause();
    }else {
      //console.log('No hay sonido')
    }

  }

  primerasCordenadas(event,item){
    //console.log(event.y)
    this.posicionSonido=item.posicion;
    this.x1c= event.x;
    this.y1c=event.y-120;
    this.probar()
  }
  segundasCoordenadas(event, item){
    this.posicionImagen=item.posicion;
    //console.log(event.y);
    this.x2c= event.x;
    this.y2c=event.y-120;
    this.probar()
  }

  probar(){

    if(this.x1c==null && this.y1c==null && this.x2c==null && this.y2c==null){
      this.messageService.add({key: 'myKey1', severity:'info', summary: 'Empieza el Juego', detail: "Seleccione un sonido y una imagen" });
      //console.log("Seleccione un sonido y una imagen")
    }else if(this.x1c==null && this.y1c==null){
      this.messageService.add({key: 'myKey1', severity:'info', summary: 'Mensaje', detail: "Seleccione un sonido" });
      //console.log("Seleccione primeras coordenadas")
    }else if(this.x2c==null && this.y2c==null ){
      this.messageService.add({key: 'myKey1', severity:'info', summary: 'Mensaje', detail: "Seleccione una imagen" });
      //console.log("Seleccione segundas coordenadas")
    }else if(this.x1c!=null && this.y1c!=null && this.x2c!=null && this.y2c!=null) {
      //console.log(this.posicionImagen, this.posicionSonido);

      if (this.posicionSonido == this.posicionImagen){

      if(this.arrayPosicionImagen.length===0 && this.arrayPosicionSonido.length===0) {
        this.arrayPosicionSonido.push(this.posicionSonido);
        this.arrayPosicionImagen.push(this.posicionImagen);

        this.prueba= {
          "x1": this.x1c,
          "y1": this.y1c,
          "x2": this.x2c,
          "y2": this.y2c,
        };
        this.coordenadas.push(this.prueba);
        //console.log(this.coordenadas);

        //console.log(this.arrayPosicionSonido, this.arrayPosicionImagen);


        this.x1c=null;
        this.y1c=null;
        this.x2c=null;
        this.y2c=null;

      }
      else if (this.arrayPosicionImagen.length>0 && this.arrayPosicionSonido.length>0){

       // console.log('entro');

        let resI = this.arrayPosicionImagen.find(o=>o ===this.posicionImagen);
        let resS = this.arrayPosicionSonido.find(o=>o ===this.posicionSonido);

       // console.log("PRUEBA",resI, resS);
        //console.log(this.arrayPosicionSonido, this.arrayPosicionImagen);


        if(resI===undefined  && resS===undefined ){
          this.arrayPosicionSonido.push(this.posicionSonido);
          this.arrayPosicionImagen.push(this.posicionImagen);


          this.prueba= {
            "x1": this.x1c,
            "y1": this.y1c,
            "x2": this.x2c,
            "y2": this.y2c,
          };
          this.coordenadas.push(this.prueba);

          if(this.arrayPosicionImagen.length === this.pathSonidos.length){

           //console.log("ganamos")
            setTimeout(()=>{
              this.ganaste= true;
              //this.playAudio("../../assets/SonidosApp/Ganaste.mp3");
              setTimeout(()=>{
                this.seleccionado=null;
                this.juego=false;
                this.ganaste=false
              }, 4000)
            }, 4000)
          }
        }
        else {

          this.fallaste=true;
          this.messageService.add({key: 'myKey1', severity:'warn', summary: 'Mensaje', detail: "Escoja otra pareja" });
          this.playAudio("../../assets/SonidosApp/Error.mp3");
          //console.log("Escoja otra pareja")
        }
      }
    }else{
        this.fallaste=true;
        this.error=true
        setTimeout(()=>{
          this.error=false
        }, 1500)
        //this.messageService.add({key: 'myKey1', severity:'error', summary: 'Mensaje', detail: "Opción Incorrecta" });
        this.playAudio("../../assets/SonidosApp/Error.mp3");

      }
    }
  }

  combinaciones(){
    //console.log('entramos')

  }


}












