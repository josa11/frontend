import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  mostrar :boolean=false;


  constructor() { }

  ngOnInit() {
  }

  cambiar(){
    this.mostrar=!this.mostrar;
  }

}
