import {Observable} from "rxjs";

export class Secuencia {

  id ?: string;
  identificador ?: number;
  tipo ?: string;
  nombre ?: string;
  estado?:string;
  fecha ?: string;
  imagenes ?: Observable<any[]>;
  sonidos ?: Observable<any[]>;

}
