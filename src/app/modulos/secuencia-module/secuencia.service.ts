import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpEvent, HttpEventType, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable, throwError} from "rxjs";
import {Terapeuta} from "../terapeuta-module/interfaces/terapeuta.model";
import {Secuencia} from "./interfaces/secuencia.model";
import {catchError, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class SecuenciaService{

  constructor(
    private http: HttpClient
  ) {}

  private getHeaders() {
    let headers = new HttpHeaders();
    headers.append( 'Accept', 'application/json' );
    headers.append( 'Content-Type', 'application/json' );
    return headers;
  }

  obtenerUno(id: string): Observable<Terapeuta>{
    return this.http.get(environment.apiBaseUrl + `/secuencia/${id}` , { headers: this.getHeaders() } )
  }

  obtenerIdentificador(): Observable<Secuencia>{
    return this.http.get(environment.apiBaseUrl + `/secuencia/obtener`, { headers: this.getHeaders() })
  }


  crear(secuencia: Secuencia):Observable<Secuencia>{
    return this.http.post(environment.apiBaseUrl + '/secuencia/', secuencia, { headers: this.getHeaders() })
  }

  actualizar(secuencia: Secuencia):Observable<Secuencia>{
    return this.http.put(environment.apiBaseUrl + '/secuencia/', secuencia, { headers: this.getHeaders() })
  }


  obtenerTodos():Observable<any>{
    return  this.http.get(environment.apiBaseUrl+'/secuencia/', { headers: this.getHeaders() })
  }

  obtenerImagenes(id):Observable<any>{
    return this.http.get(environment.apiBaseUrl+'/secuencia/'+id+'/imagenes/', { headers: this.getHeaders() })

  }

  obtenerImagen(id):Observable<any>{
    return this.http.get(environment.apiBaseUrl+'/imagen?id='+id, { headers: this.getHeaders() });
  }

  obtenerSonidos(id):Observable<any>{
    return this.http.get(environment.apiBaseUrl+'/secuencia/'+id+'/sonidos/', { headers: this.getHeaders() })
  }

  obtenerSonido(id):Observable<any>{
    return this.http.get(environment.apiBaseUrl+'/sonido?id='+id, { headers: this.getHeaders() });
  }

  obtenerInfoImagen(id):Observable<any>{
    return this.http.get(environment.apiBaseUrl+'/imagen/'+id, { headers: this.getHeaders() });
  }

  obtenerNombreSecuencia(nombre):Observable<any>{
    return this.http.put(environment.apiBaseUrl + '/secuencia/buscar/', nombre, { headers: this.getHeaders() });
  }


}
