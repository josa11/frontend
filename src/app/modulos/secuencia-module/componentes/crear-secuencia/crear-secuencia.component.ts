import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Secuencia} from "../../interfaces/secuencia.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Event, Router} from "@angular/router";
import {ToasterService} from "angular2-toaster";
import {MessageService} from "primeng/api";
import {SecuenciaService} from "../../secuencia.service";
import {HttpClient, HttpEvent, HttpRequest, HttpResponse} from "@angular/common/http";
import {FileItem, FileUploader} from "ng2-file-upload";
import {environment} from "../../../../../environments/environment";



//const URL = environment.apiBaseUrl +'/imagen/upload';
@Component({
  selector: 'app-crear-secuencia',
  templateUrl: './crear-secuencia.component.html',
  styleUrls: ['./crear-secuencia.component.css'],
  providers:[SecuenciaService, MessageService]
})
export class CrearSecuenciaComponent implements OnInit {

  secuencia: Secuencia = {};
  esFormularioValido = false;
  formularioCrearSecuencia: FormGroup;
  prueba
  checked: boolean = true;
  URL;
  URL2;
  recorrer;
  datosLlenos=false;
  bc=[]
  validName

  @ViewChild("miInput") nombre: ElementRef;

  //Imagenes
  public uploader = new FileUploader({url: this.URL, itemAlias:'upFile'});

  public uploader2 = new FileUploader({url: this.URL2, itemAlias:'upFile'});
  private btnDes= true;


  constructor(
    private readonly secuenciaService: SecuenciaService,
    private readonly formBuilder: FormBuilder,
    private router: Router,
    private readonly toasterService: ToasterService,
    private messageService: MessageService,
    private readonly activateRoute: ActivatedRoute,
    public httpClient:HttpClient
  ) {

  }

  ngOnInit(){

    this.bc.push({key:'Inicio', value:'/home-admin'}, {key:'Listar Secuencia', value:'/listar-secuencia'},{key:'Crear Secuencia', value:'/crear-secuencia'})
    localStorage.setItem('rutas',JSON.stringify(this.bc))

  }


  obtenerValores(){
    this.secuencia.identificador=this.formularioCrearSecuencia.get('identificador').value;
    this.secuencia.tipo=this.formularioCrearSecuencia.get('tipo').value;
    this.secuencia.nombre=this.formularioCrearSecuencia.get('nombre').value;
    this.secuencia.estado=this.formularioCrearSecuencia.get('estado').value;

    if(this.secuencia){
      this.datosLlenos=true
      this.btnDes=!this.btnDes;
    }
    else{
      //console.log('No se pudo')
    }

  }

  crearSecuencia() {

    if(this.uploader.queue.length >=2 && this.uploader2.queue.length>=2 && this.uploader.queue.length<=6 && this.uploader2.queue.length<=6){
      if(this.uploader.queue.length===this.uploader2.queue.length) {
        this.secuenciaService.crear(this.secuencia).subscribe(respuesta => {
            this.URL = environment.apiBaseUrl + '/imagen/upload?imagen=' + respuesta.id + '&tipo=' + respuesta.tipo + '&identificador=' + respuesta.identificador;
            this.URL2 = environment.apiBaseUrl + '/sonido/upload?sonido=' + respuesta.id + '&tipo=' + respuesta.tipo + '&identificador=' + respuesta.identificador;

            this.uploader.onBeforeUploadItem = (item: FileItem): any => {
              // logic of connecting url with the file
              item.url = this.URL;
              return {item};
            };
            this.uploader2.onBeforeUploadItem = (item: FileItem): any => {
              // logic of connecting url with the file
              item.url = this.URL2;
              return {item};
            };

            this.uploader.uploadAll();
            this.uploader2.uploadAll();

            this.uploader.onCompleteAll=()=>{
              console.log('complete')
            }

            this.uploader2.onCompleteAll=()=>{
              console.log('complete')
              this.irListar();
            }

            this.messageService.add({
              key: 'myKey1',
              severity: 'success',
              summary: 'Registro Exitoso',
              detail: 'Secuencia Creada'
              
            });
            //console.log(this.secuencia);
          },
          error => {
            this.messageService.add({
              key: 'myKey1',
              severity: 'error',
              summary: 'Registro Fallido',
              detail: 'No se creo Secuencia'
            });
            //console.log(this.secuencia.id + 'aaa' + error)
          })
      }
      else{
        this.messageService.add({key: 'myKey1',severity:'error', summary:'Error', detail:'Se debe ingresar el mismo número de imagenes y sonidos'});
      }
    }else {
      if(this.uploader2.queue.length <2) {
        this.messageService.add({key: 'myKey1',severity:'error', summary:'Error', detail:'Se debe ingresar mínimo 2 sonidos'});
        }
      if(this.uploader.queue.length<2){
        this.messageService.add({key: 'myKey1',severity:'error', summary:'Error', detail:'Se debe ingresar mínimo 2 imagenes'});
      }
      if(this.uploader2.queue.length >6) {
        this.messageService.add({key: 'myKey1',severity:'error', summary:'Error', detail:'Se debe ingresar máximo 6 sonidos'});
      }
      if(this.uploader.queue.length>6){
        this.messageService.add({key: 'myKey1',severity:'error', summary:'Error', detail:'Se debe ingresar máximo 6 imagenes'});
      }
    }
  }

  crearFormulario() {
    this.formularioCrearSecuencia = this.formBuilder.group({})
  }

  escucharFormulario(eventoFormulario: FormGroup) {
    this.formularioCrearSecuencia = eventoFormulario;
    this.esFormularioValido = this.formularioCrearSecuencia.valid

    //console.log(this.formularioCrearSecuencia.valid)
    if(this.esFormularioValido ){
      this.btnDes=false;
    }
  }

  escucharSecuencia(secuencia: Secuencia) {

    this.secuencia = secuencia
    //console.log(this.secuencia)
  }

  irListar() {
    this.router.navigate(['/listar-secuencia'])
  }



}
