import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Secuencia} from "../../interfaces/secuencia.model";
import {SecuenciaService} from "../../secuencia.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-ver-secuencia',
  templateUrl: './ver-secuencia.component.html',
  styleUrls: ['./ver-secuencia.component.css']
})
export class VerSecuenciaComponent implements OnInit {


  secuencia: Secuencia = {};
  formularioVerDetalle: FormGroup;
  prueba;
  imagenes = [];
  sonidos = [];
  imagenesForm = [];
  sonidosForm = [];
  ejemploImagenes = [];
  ejemploSonidos = [];
  url;
  idSecuencia;
  imagenesOrdenas=[];
  sonidosOrdenados=[];
  cols;
  multimedia=[]
  bc=[]
  @ViewChild('audioOption') audioPlayerRef: ElementRef;



  constructor(
    private readonly formbuilder: FormBuilder,
    private readonly router: Router,
    private secuenciaService: SecuenciaService,
    public desinfectante: DomSanitizer,
    private activateRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {

    this.bc.push({key:'Inicio', value:'/home-admin'}, {key:'Listar Secuencia', value:'/listar-secuencia'},{key:'Ver Secuencia', value:'/ver-secuencia'})
    localStorage.setItem('rutas',JSON.stringify(this.bc))

    this.cols = [
      { field: 'imagenes', header: 'Imagenes' },
      { field: 'sonidos', header: 'sonidos' }
    ]

    this.idSecuencia = this.activateRoute.snapshot.paramMap.get('id');

    this.secuenciaService.obtenerUno(this.idSecuencia).subscribe(secuencia => {

      this.secuenciaService.obtenerImagenes(this.idSecuencia).subscribe(imagenes => {
        this.imagenes = imagenes;
        this.imagenes.map(item => {
          this.secuenciaService.obtenerImagen(item.id).subscribe(respuesta => {
            this.imagenesForm = respuesta;
            this.imagenesForm.map(item => {
              //let imagenes = '..\\..\\..\\..\\..' + item.fullPath.substring(55);
              let imagenes = '.' + item.fullPath.substring(25);
              this.url = this.desinfectante.bypassSecurityTrustResourceUrl(imagenes);
              this.ejemploImagenes.push({source: imagenes})
              this.imagenesOrdenas= this.ejemploImagenes.sort((a,b)=>{
                return a.posicion-b.posicion;
              })
            })
          })
        })
      })

      this.secuenciaService.obtenerSonidos(this.idSecuencia).subscribe(sonidos => {
        this.sonidos = sonidos;
        this.sonidos.map(item => {
          this.secuenciaService.obtenerSonido(item.id).subscribe(respuesta => {
            this.sonidosForm = respuesta;
            this.sonidosForm.map(item => {
              //let sonidos = '..\\..\\..\\..\\..' + item.fullPath.substring(55)
              let sonidos = '.' + item.fullPath.substring(25)
              this.url = this.desinfectante.bypassSecurityTrustResourceUrl(sonidos);
              this.ejemploSonidos.push({src: sonidos})
              this.sonidosOrdenados= this.ejemploSonidos.sort((a,b)=>{
                return a.posicion-b.posicion;
              })
              this.multimedia.push(this.sonidosOrdenados)
            })
            //console.log(this.multimedia);
          })
        })
      })


    })


    this.crearFormulario();
  }

  private crearFormulario() {
    this.formularioVerDetalle = this.formbuilder.group({});

  }


  escucharFormulario(eventoFormulario: FormGroup) {
    this.formularioVerDetalle = eventoFormulario
  }

  escucharSecuencia(secuencia: Secuencia) {
    this.secuencia = secuencia
  }

  irListar() {
    this.router.navigate(['/listar-secuencia']);

  }


  recorre(item){
    //console.log(item)
    return "Cosa"
  }

}
