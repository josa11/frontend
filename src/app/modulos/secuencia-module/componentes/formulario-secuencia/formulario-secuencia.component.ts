import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {Terapeuta} from "../../../terapeuta-module/interfaces/terapeuta.model";
import {Secuencia} from "../../interfaces/secuencia.model";
import {ActivatedRoute} from "@angular/router";
import {SecuenciaService} from "../../secuencia.service";
import {Observable} from "rxjs";
import {SelectItem} from "primeng/api";
import {MessageService} from "../../../../../../node_modules/primeng/api";




@Component({
  selector: 'app-formulario-secuencia',
  templateUrl: './formulario-secuencia.component.html',
  styleUrls: ['./formulario-secuencia.component.css'],
  providers: [MessageService]
})

export class FormularioSecuenciaComponent implements OnInit {

  tipos=['Seleccionar Tipo','Asociacion Imagen-Sonido','Secuencia Visual Auditiva']
  estados= ['Seleccionar Estadi','Activo','Inactivo'] ;
  imagenes;
  prueba;
  sonidos;
  recorrer;
  valido;


  @Output() esValidoFormularioSecuencia :  EventEmitter<FormGroup> = new EventEmitter()
  @Input() esFormularioCrear = false
  @Input() esFormularioEditar = false
  @Input() esFormularioVer = false

  @Output() secuenciaSeleccionada : EventEmitter<Terapeuta> = new EventEmitter()

  secuencia:Secuencia
  formularioSecuencia:FormGroup

  mensajesErroresTipo=[];
  mensajesErroresNombre=[];
  mensajesErroresEstado=[];

  private mensajesValidacionTipo={
    required: "Se debe escoger un tipo de secuencia",
  }

  private mensajesValidacionNombre={
    required: "El nombre es obligatorio",
    validacionMinLength: "El nombre debe tener al menos 4 caracteres",
    buscarNombre:'Nombre ya existe'
  }

  private mensajesValidacionEstado={
    required: "Se debe escoger un estado"
  }

  constructor(
    private readonly formBuilder: FormBuilder,
    private activateRoute: ActivatedRoute,
    private readonly secuenciaService: SecuenciaService,
    private messageService: MessageService,
  ){

  }

  ngOnInit(){
    this.crearFormulario();
    this.escucharFormulario();
    this.verificarTipoFormulario();
  }

  crearFormulario(){
    this.formularioSecuencia = this.formBuilder.group({
      identificador:['',[Validators.required]],
      tipo:['',[Validators.required]],
      nombre:['',[Validators.required, this.validacionMinLength(4)]],
      estado:['',[Validators.required]],
    })
  }

  escucharFormulario(){
    this.formularioSecuencia.valueChanges.subscribe(()=>{
      this.esValidoFormularioSecuencia.emit(this.formularioSecuencia)
      this.escucharCambiosTipo()
      this.escucharCambiosNombre()
      this.escucharCambiosEstado()
    })
  }

  verificarTipoFormulario(){
    if(this.esFormularioCrear){

      this.secuenciaService.obtenerIdentificador().subscribe(secuencia=>
      {
        this.recorrer = secuencia
        this.recorrer.map(item=>{
          this.prueba = parseInt(item.identificador)+1;
          //console.log(this.prueba);
        })

      });
    }
    if(this.esFormularioVer){
      this.buscarSecuencia();
      this.formularioSecuencia.disable()
    }
  }

  buscarSecuencia(){

    this.obtenerIdSecuencia().subscribe(respuesta=>{

      this.secuenciaService.obtenerUno(respuesta.get('id')).subscribe(secuencia=>{
        this.secuencia = secuencia;
        console.log(this.secuencia);

        this.secuenciaSeleccionada.emit(this.secuencia)
        this.llenarFormulario(this.secuencia)
        this.secuenciaService.obtenerImagenes(respuesta.get('id')).subscribe(imagenes=>{
          this.imagenes =imagenes;
          //console.log('Se guardaron Imagenes');
          //console.log(this.imagenes);
          localStorage.setItem('imagenes', JSON.stringify(this.imagenes));
        });

        this.secuenciaService.obtenerSonidos(respuesta.get('id')).subscribe(sonidos=>{
          this.sonidos =sonidos;
          //console.log('Se guardaron Sonidos');
          localStorage.setItem('sonidos', JSON.stringify(this.sonidos));
          //console.log(this.sonidos);
        })

      }, error=>{
        //console.log('Error al obtener el id de Secuencia', error);
      })
    })
  }

  llenarFormulario(secuencia: Secuencia){
    // console.log(secuencia);
    let Activo: boolean;
    let Inactivo: boolean;
    if(secuencia.estado == 'Activo'){
      Activo = true;
    }
    if(secuencia.estado == 'Inactivo'){
      Inactivo = true;
    }

    this.formularioSecuencia.patchValue({
      identificador: secuencia.identificador,
      tipo: secuencia.tipo,
      nombre: secuencia.nombre,
      estado: secuencia.estado
    })

  }

  obtenerIdSecuencia():Observable<any>{
    return this.activateRoute.paramMap
  }

  escucharCambiosTipo(){
    const tipoControl = this.formularioSecuencia.get('tipo')
    tipoControl.valueChanges.subscribe(valor=>{
      this.setearMensajesErroresTipo(tipoControl)
    })
  }

  escucharCambiosNombre(){
   const nombreControl = this.formularioSecuencia.get('nombre')
    nombreControl.valueChanges.subscribe(()=>{
      this.setearMensajesErroresNombre(nombreControl)
    })
  }

  escucharCambiosEstado(){
    const estadoControl = this.formularioSecuencia.get('estado')
    estadoControl.valueChanges.subscribe(()=>{
      this.setearMensajesErroresEstado(estadoControl)
    })
  }

  setearMensajesErroresTipo(valor:AbstractControl){
    this.mensajesErroresTipo = [];
    const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
    //console.log(esValidoCampo, valor)
    if (esValidoCampo) {
      this.mensajesErroresTipo = Object.keys(valor.errors).map(atributo => {
        return this.mensajesValidacionTipo[atributo]
      })
    }
  }

  setearMensajesErroresNombre(valor: AbstractControl) {
    this.mensajesErroresNombre = [];
    const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
    if (esValidoCampo) {
      this.mensajesErroresNombre = Object.keys(valor.errors).map(atributo => {
        return this.mensajesValidacionNombre[atributo]
      })
    }
  }

  setearMensajesErroresEstado(valor:AbstractControl){
    this.mensajesErroresEstado = [];
    const esValidoCampo = (valor.dirty || valor.touched) && valor.errors
    if (esValidoCampo) {
      this.mensajesErroresEstado = Object.keys(valor.errors).map(atributo => {
        return this.mensajesValidacionEstado[atributo]
      })
    }
  }

  validacionMinLength(longitud: number): ValidatorFn {
    return (valorInput: AbstractControl): { [atributo: string]: boolean } | null => {
      if (valorInput.value.length > longitud) {
        return null
      }
      return { 'validacionMinLength': true }
    }
  }

  buscarNombre(){
    let nombreControl = this.formularioSecuencia.get('nombre')
    let nombre={"nombre": nombreControl.value }

    this.secuenciaService.obtenerNombreSecuencia(nombre).subscribe(response=>{
      if(response.err==='Nombre ya existe'){
        this.messageService.add({key: 'myKey1',severity:'error', summary:'Error', detail:'Nombre de Secuencia ya existe'});

      }else {

        //console.log(true)
      }
    })
  }

}
