import { Component, OnInit } from '@angular/core';
import {Secuencia} from "../../interfaces/secuencia.model";
import {SecuenciaService} from "../../secuencia.service";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";


@Component({
  selector: 'app-listar-secuencia',
  templateUrl: './listar-secuencia.component.html',
  styleUrls: ['./listar-secuencia.component.css']
})
export class ListarSecuenciaComponent implements OnInit {


  arregloSecuencias : Secuencia[]=[];
  imagenes;
  numeroI=[];
  numeroS=[];
  sonidos;
  fecha;
  now;
  timer: any;
  checked=true;
  checked1=false;
  bc=[];


  constructor(
    private readonly secuenciaService: SecuenciaService,
    private activateRoute: ActivatedRoute
  ) { }


  ngOnInit() {
    this.obtenerSecuencias();
    this.bc.push({key:'Inicio', value:'/home-admin'}, {key:'Listar Secuencia', value:'/listar-secuencia'})
    localStorage.setItem('rutas',JSON.stringify(this.bc))
  }

  obtenerSecuencias(){

      this.secuenciaService.obtenerTodos().subscribe(secuencias => {
          this.arregloSecuencias = secuencias;
        //console.log(this.arregloSecuencias);
        },
        error => {
          //console.log("Error al listar secuencias", error);
        })

  }

  obtenerImagenes():Observable<any>{
    return this.activateRoute.paramMap
  }



  timeBetweenDates(item) {

   let fecha = new Date(item).getTime();

  //console.log(fecha)


   let dia =  new Date(item).getDate();
   let mes =  new Date(item).getMonth()+1;
   let anio =   new Date(item).getFullYear();

    return  dia+"/"+mes+"/"+anio;

  }

  actualizar(secuencia){

    let objetoBck;

    if(secuencia.estado=='Activo'){

      objetoBck={
        "nombre":secuencia.nombre,
        "id":secuencia.id,
        "identificador":secuencia.identificador,
        "estado":'Inactivo',
        "tipo":secuencia.tipo
      }

    }else {
      objetoBck={
        "nombre":secuencia.nombre,
        "id":secuencia.id,
        "identificador":secuencia.identificador,
        "estado":'Activo',
        "tipo":secuencia.tipo
      }
    }

    //console.log(objetoBck)


    this.secuenciaService.actualizar(objetoBck).subscribe(r=>{
      //console.log(r)
      window.location.reload();
    }, error1 => {
     // console.log(error1)
    })

  }


}
